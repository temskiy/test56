#!/usr/bin/env bash
save_last_backups=$2

files_count=$(rclone ls --config ./rclone.conf --include "$1" --min-age $2d selectel:$3 | wc -l)

if [[ $files_count -gt $save_last_backups ]]; then
  rclone delete --config ./rclone.conf --include "$1" --min-age $2d selectel:$3 >> log.txt;
fi