К заданию есть некоторые вопросы, поэтому я пока не стал писать готовый компос, а лишь описал как мне это видится и добавил уточняющие вопросы.

> Структура кластера:
> 1. Балансировщик нагрузки nginx (miftest-b1)
> 2. Веб нода - 2 штуки (miftest-w1 и miftest-w2)
> 3. База данных mysql (miftest-db1)
> 4. PHP 7.3 (организовать запуск скриптов через socket)

Пункт 4 не относится к структуре кластера. Его надо перенести в условия. Но там уже есть пункт об этом.

> Условия
> - Балансировщик равномерно распределяет нагрузку между 2мя веб нодами

В апстриме nginx обе вебноды просто прописать с одинаковым весом.

> - Рабочая папка /var/www/htdocs (доступна для записи с хостовой машины)
> - В папке /var/www/htdocs находится директория files в которой лежат несколько файлов (положите любые на ваше усмотрение)

О какой ноде речь? Подозреваю что о веб - ноде, но всё же хотелось бы не догадываться, а знать точно. Мало ли, может статику прямо с балансера раздаём.

> - В папке /var/www/htdocs лежит файл index.php который выводит phpinfo()

Содержимое файла:
```
<?php
 phpinfo();
 ?>
```

> - Организована синхронизация файлов между веб нодами находящихся в папке htdocs/files (если закачать файл на miftest-w1/htdocs/files, то он появляется и на  miftest-w2/htdocs/files с минимальными задержками)

Если весь этот кластер на одном и том же сервере запущен, то примонтировать один и тот же волюм к обоим веб-нодам или с хоста одну и ту же директорию прокинуть и ничего синхронизировать не понадобится. Да, возможно что будут конфликты....но без более делтального описания работы заранее предсказать не могу.

> - На miftest-w1 и miftest-w2 установлен nginx с конфигурацией запуска php скриптов через sockets
> - Обработка php файлов осуществляется через socket (не порт)

Не об одном ли и том же речь? Ну ладно. Но nginx сам по себе php исполнять не умеет, ему нужен или php-fpm или ppm и вот они то как раз могут прослушивать как порт, так и сокет. Nginx же одинаково умеет работать с CGI и через сокет и через порт, разница по конфигурации никакой, только указать хост/порт либо сокет. 
К тому же, а собственно например php-fpm где должен быть запущен? На хосте? На каком? Или в контейнере веб-ноды? Мы правда хотим второй процесс в докер-контейнере?

> - В php есть возможность отредактировать файл php.ini без пересборки контейнера

Что мешает зайти внутрь контейнера и отредактировать файл? Ах, да, речь же наверное о веб-нодах, а у нас их две и ходить по всем неудобно.
Если хочется удобства - прокинуть файл на хост. Но тогда надо конфиг каждой в отдельный маунт. Но опять надо править все... Тогда один конфиг на всех и всё.

> - Все ноды друг друга видят и пингуются между собой

Если все ноды в одной докер сети ( а иное не оговорено) то они автоматически будут видеть друг друга и пинговаться

> - После перезапуска контейнера mysql, данные в базе не удаляются

Опять же - волюм, либо прокидывать с хоста директорию

> - Наружу открыт только 80/443 порт

Здравый смысл конечно же подсказывает что речь про контейнер балансера... Так ли? Ну тогда примерно так:

```
ports:
  - "80:80"
  - "443:443"
```
